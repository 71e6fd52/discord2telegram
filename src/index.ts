/**
 * Welcome to Cloudflare Workers! This is your first worker.
 *
 * - Run `wrangler dev src/index.ts` in your terminal to start a development server
 * - Open a browser tab at http://localhost:8787/ to see your worker in action
 * - Run `wrangler publish src/index.ts --name my-worker` to publish your worker
 *
 * Learn more at https://developers.cloudflare.com/workers/
 */

interface ENV {
  DISCORD_URL: string;
  BOT_TOKEN: string;
}

const BOUNDARY = 'boundary';

async function getFile(id: string, token: string): Promise<Blob | undefined> {
  let path: any = await fetch(`https://api.telegram.org/bot${token}/getFile?file_id=${id}`)
    .then(response => response.json());
  if (!path['ok']) { return undefined; }
  path = path['result'];

  if (path['file_path'] == undefined) { return undefined; }
  const res = await fetch(`https://api.telegram.org/file/bot${token}/${path['file_path']}`)
  return await res.blob();
}

export default {
  async fetch(request: Request, env: ENV): Promise<Response> {

    if (!request.url.includes("8d5de5bd-01d8-42de-9730-b7edf6d7fac2")
      || request.method != 'POST') {
      return new Response(null, { status: 204 })
    }

    let data: any = await request.json();
    console.log(data);

    if (data['channel_post'] == null) {
      return new Response(null, { status: 204 })
    }

    const post = data['channel_post']

    var bodyParts: BlobBits = [];

    let content: string;
    let raw_entities: any[] | null;
    if (post['text'] != null) {
      content = post['text'];
      raw_entities = post['entities'];
    } else if (post['caption'] != null) {
      content = post['caption'];
      raw_entities = post['caption_entities'];
    } else {
      content = '';
      raw_entities = null;
    }

    let entities: Array<{ start: number; end: number; pre: string; post: string }> = []

    if (raw_entities != null && raw_entities.length > 0) {
      for (const entity of raw_entities) {
        let type;
        switch (entity['type']) {
          case 'bold':
            type = '**';
            break;
          case 'italic':
            type = '*'
            break;
          case 'underline':
            type = '__';
            break;
          case 'strikethrough':
            type = '~~';
            break;
          case 'code':
            type = '`';
            break;
          case 'pre':
            type = '\n```\n';
            break;
          case 'spoiler':
            type = '||';
            break;
          case 'text_link':
            type = 'link'
            break;
          default:
            continue;
        }
        if (type == 'link') {
          entities.push({ start: entity['offset'], end: entity['offset'] + entity['length'], pre: '[', post: `](${entity['url']})` })
        } else {
          entities.push({ start: entity['offset'], end: entity['offset'] + entity['length'], pre: type, post: type })
        }
      }

      for (const entity of entities) {
        const es = entity['start'];
        const ee = entity['end']
        content = content.slice(0, entity['end']) + entity['post'] + content.slice(entity['end'])
        content = content.slice(0, entity['start']) + entity['pre'] + content.slice(entity['start'])
        for (const e of entities) {
          if (e['start'] >= ee) {
            e['start'] += entity['pre'].length + entity['post'].length
          } else if (e['start'] > es) {
            e['start'] += entity['pre'].length
          }

          if (e['end'] >= ee) {
            e['end'] += entity['pre'].length + entity['post'].length
          } else if (e['end'] > es) {
            e['end'] += entity['pre'].length
          }
        }
      }
    }

    const send_data: {
      content: string;
      attachments: Array<{ id: number; filename: string }>;
    } = {
      content: content,
      attachments: [],
      // embeds: []
    };

    if (post['photo'] != null && post['photo'].length > 0) {
      const i = post['photo'].length - 1;
      const photo = post['photo'][i];

      // send_data.embeds.push({
      //   image: {
      //     url: `attachment://${photo['file_unique_id']}.jpg`,
      //     height: photo['height'],
      //     width: photo['width'],
      //   }
      // })
      send_data.attachments.push({
        id: 0,
        filename: photo['file_unique_id'] + '.jpg'
      })
    }

    console.log(JSON.stringify(send_data));


    bodyParts.push(
      '--' + BOUNDARY + '\r\n',
      'Content-Disposition: form-data; name="payload_json"\r\n',
      'Content-Type: application/json\r\n',
      '\r\n',
      JSON.stringify(send_data) + '\r\n');

    if (post['photo'] != null && post['photo'].length > 0) {
      const i = post['photo'].length - 1;
      const photo = post['photo'][i];

      const file = await getFile(photo['file_id'], env['BOT_TOKEN']);

      if (file != null) {
        bodyParts.push(
          '--' + BOUNDARY + '\r\n',
          `Content-Disposition: form-data; name="files[0]"; filename="${photo['file_unique_id']}.jpg"\r\n`,
          'Content-Type: image/jpeg\r\n',
          '\r\n',
          file,
          '\r\n'
        );
      }
    }
    // if (post['animation'] != null && post['animation'].length > 0) {
    //   const animation = post['animation'];

    //   const file = await getFile(animation['file_id']);

    //   bodyParts.push(
    //     '--' + BOUNDARY + '\r\n',
    //     `Content-Disposition: form-data; name="files[0]"; filename="${animation['file_unique_id']}.jpg"\r\n`,
    //     `Content-Type: ${animation['mime_type'] || 'image/gif'}\r\n`,
    //     '\r\n',
    //     file,
    //     '\r\n'
    //   );
    // }

    bodyParts.push('--' + BOUNDARY + '--\r\n', '');
    const body = new Blob(bodyParts, { type: 'multipart/form-data' });
    const headers: HeadersInit = {
      'Content-Type': 'multipart/form-data; boundary=' + BOUNDARY,
      'Content-Length': String(body.size)
    }

    console.log(await body.text());

    let send_request = new Request(env['DISCORD_URL'], { method: "POST", body: body, headers: headers });
    let dc_res = await fetch(send_request)
    if (request.url.includes("log=true")) {
      return dc_res
    } else {
      return new Response(null, { status: 204 })
    }
  },
};
